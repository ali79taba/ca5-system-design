const TimeSlotRepository = require("../dao/timeSlotRepository");

class TimeSlotController {

    getTimes(from, type) {
        const timeSlotRepository = new TimeSlotRepository();
        return timeSlotRepository.getTimes(from, type);
    }

    getTimeSlot(timeslot) {
        const timeSlotRepository = new TimeSlotRepository();
        return timeSlotRepository.getById(timeslot);
    }
}

module.exports = TimeSlotController;