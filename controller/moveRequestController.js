const MoveRequest = require("../entity/moveRequest");
const MoveRequestRepository = require("../dao/moveRequestRepository");
const PaymentService = require('../service/paymentService');
const TimeSlotController = require("./TimeSlotController");

class MoveRequestController {
    static moveRequestRepository = new MoveRequestRepository();

    static async newMoveRequest(req, res, next){
        const customer = req.query.id
        const moveRequest = MoveRequest.createMove(customer);
        await MoveRequestController.moveRequestRepository.save(moveRequest);
        res.render('new', {reqId: moveRequest.id })
    }

    static async setBasicInfo(req, res, next){
        const body = req.body;
        const reqId = body.reqId
        const moveRequest = MoveRequestController.moveRequestRepository.getById(reqId);
        moveRequest.updateData(body.from, body.to, body.type);
        await MoveRequestController.moveRequestRepository.save(moveRequest);
        const timeSlotController = new TimeSlotController();
        const timeSlots = timeSlotController.getTimes();
        res.render('timeSlots', { timeSlots, reqId: body.reqId })
    }

    static async setTime(req, res, next){
        const body = req.body;
        const reqId = body.reqId;
        const timeSlot = body.timeSlotId
        const moveRequest = MoveRequestController.moveRequestRepository.getById(reqId);
        moveRequest.updateTime(timeSlot);
        await MoveRequestController.moveRequestRepository.save(moveRequest);
        res.render('payment', { reqId: body.reqId });
    }

    static async submit(req, res, next){
        const body = req.body;
        const reqId = body.reqId;
        const moveRequest = MoveRequestController.moveRequestRepository.getById(reqId);
        const paymentStatus = PaymentService.payment(moveRequest, moveRequest.getPrice());
        if(paymentStatus === 'success'){
                moveRequest.changeSuccessState();
                await this.moveRequestRepository.save(moveRequest);
        }
        const timeSlotController = new TimeSlotController();
        const timeSlot = timeSlotController.getTimeSlot(moveRequest.timeslot);
        res.render('successful', { moveRequest, timeSlot })
    }
}

module.exports = MoveRequestController;