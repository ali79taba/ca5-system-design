const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const AbstractRepository = require("./abstractRepository");
const fs = require("fs");
const parse = require('csv-parse/lib/sync')
const MoveRequest = require("../entity/moveRequest");

class MoveRequestRepository {
    constructor() {
        // super("moveRequest.csv");
        this.file = "moveRequest.csv";
    }


    getAll() {
        const entities = {}
        const data = fs.readFileSync(this.file, 'utf8');
        const records = parse(data, {
            columns: true,
            skip_empty_lines: true
        })
        records.forEach(row => {
            const entity = new MoveRequest();
            entity.id = row._id;
            entity.customer = row._customer;
            entity.timeslot = row._timeslot;
            entity.from = row.from;
            entity.to = row.to;
            entity.type = row.type;
            entity.paymentStatus = row.paymentStatus;
            entities[entity.id] = entity;
        })
        console.log("entities : ",entities);
        return entities
    }

    async save(entity) {
        const entities = this.getAll();
        entities[entity.id] = entity;
        const csvWriter = createCsvWriter({
            path: this.file,
            header: [
                {id: '_customer', title: '_customer'},
                {id: '_id', title: '_id'},
                {id: '_timeslot', title: '_timeslot'},
                {id: 'from', title: 'from'},
                {id: 'to', title: 'to'},
                {id: 'type', title: 'type'},
                {id: 'paymentStatus', title: 'paymentStatus'}
            ]
        });

        await csvWriter.writeRecords(Object.values(entities))
    }

    getById(reqId) {
        return this.getAll()[reqId];
    }
}

module.exports = MoveRequestRepository;