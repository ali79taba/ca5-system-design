const TimeSlot = require("../entity/timeSlot");

const timeSlot1 = new TimeSlot('2021-10-09 09:00', '2021-10-09 10:00', 201);
const timeSlot2 = new TimeSlot('2021-10-09 10:00', '2021-10-09 11:00', 202);
const timeSlot3 = new TimeSlot('2021-10-09 11:00', '2021-10-09 12:00', 203);

const repo = {
    [timeSlot1.id]: timeSlot1,
    [timeSlot2.id]: timeSlot2,
    [timeSlot3.id]: timeSlot3
}

class TimeSlotRepository {

    constructor() {
    }

    getTimes(from, type) {
        return [timeSlot1, timeSlot2, timeSlot3];
    }

    getById(id){
        return repo[id];
    }
}

module.exports = TimeSlotRepository;