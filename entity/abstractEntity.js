class AbstractEntity {
    generateId() {
        return Math.floor(Math.random() * 1000000000);
    }

    constructor() {
        this._id = this.generateId();
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

}


module.exports = AbstractEntity;