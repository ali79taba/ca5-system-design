const AbstractEntity = require("./abstractEntity");

class Customer extends AbstractEntity{
    _firstName;
    _lastName;
    _phoneNumber;

    constructor() {
        super();
    }

    get firstName() {
        return this._firstName;
    }

    set firstName(value) {
        this._firstName = value;
    }

    get lastName() {
        return this._lastName;
    }

    set lastName(value) {
        this._lastName = value;
    }

    get phoneNumber() {
        return this._phoneNumber;
    }

    set phoneNumber(value) {
        this._phoneNumber = value;
    }


}