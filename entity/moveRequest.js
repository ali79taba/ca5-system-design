const AbstractEntity = require("./abstractEntity");

class MoveRequest extends AbstractEntity{
    constructor() {
        super();
        this._customer = null;
        this._timeslot = null;
        this.from = null;
        this.to = null;
        this.type = null;
        this.paymentStatus = 'unpaid';
    }

    static createMove(customer){
        const moveRequest = new MoveRequest();
        moveRequest.customer = customer;
        return moveRequest;
    }

    updateData(from, to, type) {
        this.from = from;
        this.to = to;
        this.type = type;
    }

    updateTime(timeslot){
        this._timeslot = timeslot;
    }

    get customer() {
        return this._customer;
    }

    set customer(value) {
        this._customer = value;
    }

    get timeslot() {
        return this._timeslot;
    }

    set timeslot(value) {
        this._timeslot = value;
    }

    getPrice(){
        return Math.floor(Math.random() * 10000);
    }

    changeSuccessState(){
        this.paymentStatus = 'paid';
    }

}

module.exports = MoveRequest;