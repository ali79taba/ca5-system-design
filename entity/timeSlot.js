const AbstractEntity = require("./abstractEntity");

class TimeSlot extends AbstractEntity {
    _car;
    _start;
    _end;
    constructor(start, end, car) {
        super();
        this._car = car;
        this._start = start;
        this._end = end;
    }


    get car() {
        return this._car;
    }

    set car(value) {
        this._car = value;
    }

    get start() {
        return this._start;
    }

    set start(value) {
        this._start = value;
    }

    get end() {
        return this._end;
    }

    set end(value) {
        this._end = value;
    }
}

module.exports = TimeSlot;