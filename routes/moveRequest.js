var MoveRequest = require('../entity/moveRequest');
var MoveRequestRepository = require('../dao/moveRequestRepository.js')
var express = require('express');
const MoveRequestController = require("../controller/moveRequestController");
const TimeSlotController = require("../controller/TimeSlotController");
var router = express.Router();

router.get('/new', MoveRequestController.newMoveRequest);

router.post("/setBasicInfo", MoveRequestController.setBasicInfo)

router.post("/setTimeSlot", MoveRequestController.setTime)

router.post("/submit", MoveRequestController.submit)

module.exports = router;
